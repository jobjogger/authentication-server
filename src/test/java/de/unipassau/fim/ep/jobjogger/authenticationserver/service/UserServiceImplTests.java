package de.unipassau.fim.ep.jobjogger.authenticationserver.service;

import static org.junit.Assert.assertEquals;
import de.unipassau.fim.ep.jobjogger.authenticationserver.model.User;
import de.unipassau.fim.ep.jobjogger.authenticationserver.repository.RoleRepository;
import de.unipassau.fim.ep.jobjogger.authenticationserver.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.security.crypto.password.PasswordEncoder;

@RunWith(MockitoJUnitRunner.class)
@AutoConfigureTestDatabase
public class UserServiceImplTests {

    @Mock
    UserRepository userRepository;

    @Mock
    RoleRepository roleRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    private UserServiceImpl userService;

    @Before
    public void setup() {
        userService = new UserServiceImpl(userRepository, roleRepository, passwordEncoder);
    }

    @Test
    public void saveTest() {
        Long id = 23L;
        User test = new User();
        test.setId(id);
        test.setUsername("test");

        Mockito.when(userRepository.findByUsername("test")).thenReturn(test);

        userService.saveUser(test);

        assertEquals(userService.findByUsername("test"), test);
    }

    @Test
    public void findByUsernameTest() {
        User a = new User();
        a.setUsername("test");

        Mockito.when(userRepository.findByUsername("test")).thenReturn(a);

        User retrievedUser = userService.findByUsername("test");
        assertEquals(a, retrievedUser);
    }

}
