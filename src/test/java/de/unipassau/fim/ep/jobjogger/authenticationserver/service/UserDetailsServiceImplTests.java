package de.unipassau.fim.ep.jobjogger.authenticationserver.service;

import de.unipassau.fim.ep.jobjogger.authenticationserver.model.Role;
import de.unipassau.fim.ep.jobjogger.authenticationserver.model.User;
import de.unipassau.fim.ep.jobjogger.authenticationserver.repository.UserRepository;
import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@RunWith(MockitoJUnitRunner.class)
@AutoConfigureTestDatabase
public class UserDetailsServiceImplTests {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserDetailsServiceImpl userDetailsService;

    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Before
    public void setup() {
        userDetailsServiceImpl = new UserDetailsServiceImpl(userRepository);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testUserNotFound() {
        Mockito.when(userRepository.findByUsername("test")).thenReturn(null);
        userDetailsServiceImpl.loadUserByUsername("test");
    }

    @Test
    public void testValidUser() {
        User test = new User();
        test.setUsername("test");
        test.setPassword("123");
        Role userRole = new Role();
        userRole.setName("role");
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(userRole);
        test.setRoles(roleSet);
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(userRole.getName()));


        Mockito.when(userRepository.findByUsername("test")).thenReturn(test);

        UserDetails expected = new org.springframework.security.core.userdetails
            .User(test.getUsername(), test.getPassword(), grantedAuthorities);
        assertEquals(expected, userDetailsServiceImpl.loadUserByUsername("test"));
    }

}
