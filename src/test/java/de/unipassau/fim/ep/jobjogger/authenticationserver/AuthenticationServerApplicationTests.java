package de.unipassau.fim.ep.jobjogger.authenticationserver;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureTestDatabase
class AuthenticationServerApplicationTests {

	@Test
	void contextLoads() {
	}

}
