package de.unipassau.fim.ep.jobjogger.authenticationserver.web;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.unipassau.fim.ep.jobjogger.authenticationserver.constraint.FieldMatchValidator;
import de.unipassau.fim.ep.jobjogger.authenticationserver.model.User;
import de.unipassau.fim.ep.jobjogger.authenticationserver.model.UserDTO;
import de.unipassau.fim.ep.jobjogger.authenticationserver.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

@Import(FieldMatchValidator.class)
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.MOCK)
public class UserRegistrationControllerTests {

    @Autowired
    private ModelMapper mapper;

    private BindingResult result = mock(BindingResult.class);

    @MockBean
    private UserService userService;

    private UserRegistrationController userRegistrationController;

    @Before
    public void before(){
        userRegistrationController = new UserRegistrationController(userService, mapper);
    }

    @Test
    public void showRegistrationFormTest() {
        Model model = new ExtendedModelMap();
        String returnValue = userRegistrationController.showRegistrationForm(model);
        assertEquals("registration", returnValue);
    }

    @Test
    public void testRegister() throws Exception {
        UserDTO dto = new UserDTO();
        dto.setUsername("hans");
        Model model = new ExtendedModelMap();

        String returnValue = userRegistrationController
            .registerUserAccount(dto, result, model);
        assertEquals("redirect:/registration?success", returnValue);
    }

    @Test
    public void testUserExisting() throws Exception {
        UserDTO dto = new UserDTO();
        dto.setUsername("hans");
        Model model = new ExtendedModelMap();

        when(userService.findByUsername(anyString())).thenReturn(new User());

        String returnValue = userRegistrationController
            .registerUserAccount(dto, result, model);
        assertEquals("redirect:/registration?error=The username is already taken", returnValue);
    }

}
