package de.unipassau.fim.ep.jobjogger.authenticationserver.web;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureTestDatabase
public class MainControllerTests {

    private MockMvc mockMvc;

    @Autowired
    private ModelMapper modelMapper;

    private MainController mainController;

    @Before
    public void before(){
        mainController = new MainController();
    }

    @Test
    public void testLogin() throws Exception {
        Model model = new ExtendedModelMap();
        String returnValue = mainController.login(model);
        assertEquals("login", returnValue);
    }
}
