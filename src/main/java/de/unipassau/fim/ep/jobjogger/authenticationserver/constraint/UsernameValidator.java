package de.unipassau.fim.ep.jobjogger.authenticationserver.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * ConstraintValidator to handle username validation
 */
public class UsernameValidator implements ConstraintValidator<DisallowedUsernames, String> {
    protected String[] forbiddenUsernames;

    /**
     * Initializes validator with forbidden usernames
     *
     * @param disallowedUsernames users who are not allowed
     */
    @Override
    public void initialize(DisallowedUsernames disallowedUsernames) {
        this.forbiddenUsernames = disallowedUsernames.value();

    }

    /**
     * Method to check if username is valid
     *
     * @param username username
     * @param constraintValidatorContext the validator to handle validation
     * @return true if username is valid
     */
    @Override
    public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {
        for (String illegalUsername : forbiddenUsernames) {
            if (illegalUsername.equals(username)) {
                return false;
            }
        }
        return true;
    }
}
