package de.unipassau.fim.ep.jobjogger.authenticationserver.web;

import de.unipassau.fim.ep.jobjogger.authenticationserver.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * Controller for handling request regarding the user.
 */
@Slf4j
@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Controller for getting the user's email address as String.
     */
    @GetMapping("/mail/{username}")
    public ResponseEntity<String> getMailAddress(@PathVariable String username, @RequestHeader(value="Authorization") String header) {
       return userService.getMailAddress(username, header);
    }
}
