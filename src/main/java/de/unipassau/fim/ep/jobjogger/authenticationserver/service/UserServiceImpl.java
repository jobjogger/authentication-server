package de.unipassau.fim.ep.jobjogger.authenticationserver.service;

import de.unipassau.fim.ep.jobjogger.authenticationserver.model.Role;
import de.unipassau.fim.ep.jobjogger.authenticationserver.model.User;
import de.unipassau.fim.ep.jobjogger.authenticationserver.repository.RoleRepository;
import de.unipassau.fim.ep.jobjogger.authenticationserver.repository.UserRepository;
import java.util.HashSet;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Implementation of the user service interface
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    @Value("${microservice-secret.secret-token}")
    private String principalRequestValue;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Saves a new User.
     *
     * @param user to save.
     */
    @Override
    public void saveUser(User user) {
        setRole(user,"ROLE_USER");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    /**
     * Saves a admin user
     *
     * @param admin user to be created
     */
    @Override
    public void saveAdmin(User admin) {
        setRole(admin,"ROLE_ADMIN");
        setRole(admin, "ROLE_USER");
        admin.setPassword(passwordEncoder.encode(admin.getPassword()));
        userRepository.save(admin);
    }

    /**
     * Sets the roles of a user.
     */
    private void setRole(User user, String role) {
        Role roleEntity = roleRepository.findByName(role);
        Set<Role> roleSet = user.getRoles();
        if(roleSet == null){
            roleSet = new HashSet<>();
        }
        if (roleEntity == null) {
            Role userRole = new Role();
            userRole.setName(role);
            roleRepository.save(userRole);
            roleSet.add(userRole);
        } else{
            roleSet.add(roleEntity);
        }
        user.setRoles(roleSet);
    }

    /**
     * Find user by username.
     *
     * @param username name of the user to find.
     * @return The user with the specific username.
     */
    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    /**
     * Gets the user's mail-address if the micro-service secret, stored in the request header matches
     * the micro-service secret, stored in the auth-server.
     *
     * @param username The name of the user.
     * @param header The header of the request.
     */
    @Override
    public ResponseEntity<String> getMailAddress(String username, String header) {
        User existing = findByUsername(username);

        if (existing == null) {
            log.info("Not Found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Got following user's e-mail address from mail controller: " + existing.getEmail());
        log.info("Got following header from mail controller: " + header);
        String[] headerSplit = header.split(" ");

        if (!principalRequestValue.equals(headerSplit[1])) {
            log.info("Forbidden: Wrong microservice secret in request... " +
                    "VALUE:  + " + principalRequestValue + ", Principal: " + headerSplit[1]);
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            log.info("Getting mail address has been successful.");
            return new ResponseEntity<>(existing.getEmail(), HttpStatus.OK);
        }
    }
}