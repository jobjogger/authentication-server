package de.unipassau.fim.ep.jobjogger.authenticationserver.web;

import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Tokencontroller to manage token deletion
 */
@RestController
@Slf4j
public class TokenController {

    @Autowired
    private DefaultTokenServices tokenServices;

    /**
     * Method to extract and remove token from tokenstore
     *
     * @param request to the endpoint
     */
    @CrossOrigin(origins = "*", methods = {RequestMethod.DELETE})
    @RequestMapping(method = RequestMethod.DELETE, path = "/oauth/revoke_token")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void revokeToken(HttpServletRequest request) {
        String authorization = request.getHeader("Authorization");
        log.info("TokenController: " + authorization);
        if (authorization != null && authorization.contains("Bearer")){
            String token = authorization.substring("Bearer".length()+1);
            log.info("TokenController - token: " + token);
            tokenServices.revokeToken(token);
        }
    }
}
