package de.unipassau.fim.ep.jobjogger.authenticationserver.web;

import de.unipassau.fim.ep.jobjogger.authenticationserver.model.User;
import de.unipassau.fim.ep.jobjogger.authenticationserver.model.UserDTO;
import de.unipassau.fim.ep.jobjogger.authenticationserver.service.UserService;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller to handle user registration
 */
@Controller
@RequestMapping("/registration")
@Validated
public class UserRegistrationController {

    @Value("${registration.js.src}")
    private String jsUrl;

    @Value("${registration.css.src}")
    private String cssUrl;

    private final UserService userService;

    private final ModelMapper modelMapper;

    @Autowired
    public UserRegistrationController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @ModelAttribute("user")
    public UserDTO userRegistrationDto() {
        return new UserDTO();
    }

    /**
     * Returns registration view and adds env variables.
     *
     * @param model the model object.
     * @return the registration form page.
     */
    @GetMapping
    public String showRegistrationForm(Model model) {
        model.addAttribute("jsUrl", jsUrl);
        model.addAttribute("cssUrl", cssUrl);
        return "registration";
    }

    /**
     * Registers a new User account.
     *
     * @param userDto User object.
     * @param result Binding result.
     * @param model model template
     * @return result
     */
    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid UserDTO userDto,
        BindingResult result, Model model) {
        User existing = userService.findByUsername(userDto.getUsername());
        model.addAttribute("jsUrl", jsUrl);
        model.addAttribute("cssUrl", cssUrl);
        if (existing != null) {
            return "redirect:/registration?error=" + "The username is already taken";
        }

        User newUser = modelMapper.map(userDto, User.class);
        userService.saveUser(newUser);

        return "redirect:/registration?success";
    }
}
