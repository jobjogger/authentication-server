package de.unipassau.fim.ep.jobjogger.authenticationserver.service;

import de.unipassau.fim.ep.jobjogger.authenticationserver.model.User;
import org.springframework.http.ResponseEntity;

/**
 * User service interface
 */
public interface UserService {

        /**
         * Save user with user-role to register him.
         *
         * @param user the user which should be created.
         */
        void saveUser(User user);

        /**
         * Save user with admin-role
         *
         * @param admin user to be created
         */
        void saveAdmin(User admin);

        /**
         * Finds a user by name
         *
         * @param username username
         * @return user
         */
        User findByUsername(String username);

        /**
         * Get mail address from user
         *
         * @param username username
         * @param header request header
         * @return email
         */
        ResponseEntity<String> getMailAddress(String username, String header);
        }