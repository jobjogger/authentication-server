package de.unipassau.fim.ep.jobjogger.authenticationserver.repository;

import de.unipassau.fim.ep.jobjogger.authenticationserver.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository to store user roles
 */
public interface RoleRepository extends JpaRepository<Role, Long>{
    Role findByName(String name);
}
