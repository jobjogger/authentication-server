package de.unipassau.fim.ep.jobjogger.authenticationserver.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Main controller which handles login page
 */
@Controller
public class MainController {

    @Value("${login.js.src}")
    private String jsUrl;

    @Value("${login.css.src}")
    private String cssUrl;

    /**
     * Returns the login Page and adds env variables.
     *
     * @param model model which holds attributes
     * @return Login Page
     */
    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("jsUrl", jsUrl);
        model.addAttribute("cssUrl", cssUrl);
        return "login";
    }
}
