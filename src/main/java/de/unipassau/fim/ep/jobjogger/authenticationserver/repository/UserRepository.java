package de.unipassau.fim.ep.jobjogger.authenticationserver.repository;

import de.unipassau.fim.ep.jobjogger.authenticationserver.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository to store users
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}