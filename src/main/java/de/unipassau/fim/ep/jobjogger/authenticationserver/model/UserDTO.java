package de.unipassau.fim.ep.jobjogger.authenticationserver.model;

import de.unipassau.fim.ep.jobjogger.authenticationserver.constraint.DisallowedUsernames;
import de.unipassau.fim.ep.jobjogger.authenticationserver.constraint.FieldMatch;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Length;

@FieldMatch.List({
        @FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match"),
        @FieldMatch(first = "email", second = "confirmEmail", message = "The email fields must match")
})

/**
 * DTO for a user
 */
public class UserDTO {

    @NotEmpty
    @DisallowedUsernames(value = {"default", "system", "admin", "root", "kube-system", "kube-node-lease", "kube-public"})
    @Pattern(regexp = "^[a-z0-9-.]+$")
    @Size(min = 3, max = 32)
    private String username;

    @NotEmpty
    @Length(min = 6)
    private String password;

    @NotEmpty
    private String confirmPassword;

    @Email
    @NotEmpty
    private String email;

    @AssertTrue
    private Boolean terms;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getTerms() {
        return terms;
    }

    public void setTerms(Boolean terms) {
        this.terms = terms;
    }

}