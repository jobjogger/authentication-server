package de.unipassau.fim.ep.jobjogger.authenticationserver.config;

import de.unipassau.fim.ep.jobjogger.authenticationserver.model.User;
import de.unipassau.fim.ep.jobjogger.authenticationserver.repository.RoleRepository;
import de.unipassau.fim.ep.jobjogger.authenticationserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Initializer to create 2 sample users
 */
@Component
public class DbInitializer implements CommandLineRunner {

    final
    UserService userService;

    final
    PasswordEncoder passwordEncoder;

    final
    RoleRepository roleRepository;

    private final String USER_NAME = "vali";
    private final String ADMIN_NAME = "nicole";


    @Autowired
    public DbInitializer(UserService userService, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    /**
     * Creates 2 users
     *
     * @param args incoming main method arguments
     * @throws Exception on error
     */
    @Override
    public void run(String... args) throws Exception {
        User user = new User();
        user.setUsername(USER_NAME);
        user.setPassword("1234");
        user.setEmail("valentin@web.de");

        if (userService.findByUsername(USER_NAME) == null) {
            userService.saveUser(user);
        }

        User admin = new User();
        admin.setUsername(ADMIN_NAME);
        admin.setPassword("password");
        admin.setEmail("admin@web.de");

        if (userService.findByUsername(ADMIN_NAME) == null) {
            userService.saveAdmin(admin);
        }
    }
}
