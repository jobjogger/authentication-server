# JobJogger Authentication Server
This repository contains the code of our JobJogger Authentication Server. 
It's the service that handles all the authentication and acts as the OAuth 2 Authentication Server.

## Environment Variables
| Name                     | Description                                            |
|--------------------------|--------------------------------------------------------|
| CLIENT_SECRET            | OAuth 2 client secret                                  |
| CLIENT_ID                | OAuth 2 client id                                      |
| CLIENT_HOST              | Hostname of the Client                                 |
| RESOURCE_SERVER_HOST     | Hostname of the Resource Server                        |
| LOGIN_JS                 | The url of the login screen JS                         |
| LOGIN_CSS                | The url of the login screen CSS                        |
| REGISTRATION_JS          | The url of the registration screen JS                  |
| REGISTRATION_CSS         | The url of the registration screen CSS                 |
| MICROSERVICE_COMM_SECRET | Secret for internal communication between the services |
| SPRING_DATASOURCE_URL    | URL of the Database to be used                         |
| SPRING_DATASOURCE_USER   | Username for the Database                              |
| SPRING_DATASOURCE_PASS   | Password for the Database                              |
